package co.com.west.rest.webservices.restfulwebservices.user;

import co.com.west.rest.webservices.restfulwebservices.exception.PostNotFoundException;
import co.com.west.rest.webservices.restfulwebservices.exception.UserNotFoundException;
import co.com.west.rest.webservices.restfulwebservices.post.Post;
import co.com.west.rest.webservices.restfulwebservices.post.PostDaoService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;

@RestController
public class UserResource {

    @Autowired
    private UserDaoService service;

    @Autowired
    private PostDaoService postDaoService;

    @GetMapping("/users")
    @ApiOperation(value = "retrieveAllUsers", notes = "Retrieve all users, generate a exception when users not exist.")
    public List<User> retrieveAllUsers() {
        List<User> all = service.findAll();
        if (all == null || all.isEmpty()) {
            throw new UserNotFoundException("Not exists users.");
        }
        return all;
    }

    @GetMapping("/users/{id}")
    public Resource<User> retrieveUser(@PathVariable int id) {
        User user = service.findOne(id);
        if (user == null) {
            throw new UserNotFoundException("id-" + id);
        }

        /* using springboot 2.3.x
        WebMvcLinkBuilder linkTo = WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(this.getClass()).retrieveAllUsers());

        user.add(linkTo.withRel("all-users"));

        return user;*/

        Resource<User> resource = new Resource<User>(user);
        ControllerLinkBuilder linkTo =
                ControllerLinkBuilder.linkTo(ControllerLinkBuilder.methodOn(this.getClass()).retrieveAllUsers());
        resource.add(linkTo.withRel("all-users"));
        return resource;
    }

    @DeleteMapping("/users/{id}")
    public void deleteUser(@PathVariable int id) {
        User user = service.deleteById(id);
        if (user == null) {
            throw new UserNotFoundException("id-" + id);
        }
    }

    @PostMapping("/users")
    public ResponseEntity<User> createUser(@Valid @RequestBody User user) {
        try {
            User savedUser = service.save(user);
            URI location = ServletUriComponentsBuilder
                    .fromCurrentRequest()
                    .path("/{id}")
                    .buildAndExpand(savedUser.getId()).toUri();
            return ResponseEntity.created(location).build();
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping("/users/{id}/posts")
    public List<Post> retrieveAllPostByUser(@PathVariable int id) {
        List<Post> allPostByUser = postDaoService.findAllPostByUser(id);
        if (allPostByUser == null || allPostByUser.isEmpty()) {
            throw new PostNotFoundException("id-" + id);
        }
        return allPostByUser;
    }

    @GetMapping("/users/{id}/posts/{post_id}")
    public Post retrievePostByUser(@PathVariable int id, @PathVariable int post_id) {
        Post post = postDaoService.findOne(id, post_id);
        if (post == null) {
            throw new PostNotFoundException("id-" + id + " - post-id-" + post_id);
        }
        return post;
    }

    @PostMapping("/users/{id}/posts")
    public ResponseEntity<Post> createPost(@PathVariable int id, @RequestBody Post post) {
        User user = service.findOne(id);
        try {
            if (user == null) {
                throw new UserNotFoundException("id-" + id);
            }
            post.setUser(user);
            Post savedPost = postDaoService.save(post);
            URI location = ServletUriComponentsBuilder
                    .fromCurrentRequest()
                    .path("/{post_id}")
                    .buildAndExpand(savedPost.getId_post()).toUri();
            return ResponseEntity.created(location).build();
        } catch (UserNotFoundException unfe) {
            throw unfe;
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }


}
