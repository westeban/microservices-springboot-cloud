package co.com.west.rest.webservices.restfulwebservices;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    private static final Contact DEFAULT_INFO_CONTACT = new Contact(
            "Wilson Velez","http://west.com.co","west7889@gmail.com");

    private static final ApiInfo API_INFO_DEFAULT = new ApiInfo(
            "Awesome API Title", "Awesome API Documentation", "1.0",
            "urn:tos", DEFAULT_INFO_CONTACT, "Apache 2.0",
            "http://www.apache.org/licenses/LICENSE-2.0", new ArrayList<>());

    private static final Set<String> DEFAULT_PRODUCES_AND_CONSUMES =
            new HashSet<String>(Arrays.asList("application/json", "application/xml"));

    //LinkDiscovers are needs to fix problems of incompatibility with Hateoas using with springboot 2.3.x
   /*@Bean
   public LinkDiscoverers discoverers() {
       List<LinkDiscoverer> plugins = new ArrayList<>();
       plugins.add(new CollectionJsonLinkDiscoverer());
       return new LinkDiscoverers(SimplePluginRegistry.create(plugins));
   }*/

    @Bean
    public Docket api(){
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(API_INFO_DEFAULT)
                .produces(DEFAULT_PRODUCES_AND_CONSUMES)
                .consumes(DEFAULT_PRODUCES_AND_CONSUMES);
    }
}
