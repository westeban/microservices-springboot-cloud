package co.com.west.rest.webservices.restfulwebservices.post;

import co.com.west.rest.webservices.restfulwebservices.user.User;

public class Post {

    private Integer id_post;
    private User user;
    private String message;

    public Post(Integer id_post, User user, String message) {
        this.id_post = id_post;
        this.user = user;
        this.message = message;
    }

    public Integer getId_post() {
        return id_post;
    }

    public void setId_post(Integer id_post) {
        this.id_post = id_post;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return String.format(
                "Post (id_post=%s, user=%s, message=%s)", this.id_post, this.user, this.message);
    }
}
