package co.com.west.rest.webservices.restfulwebservices.post;

import co.com.west.rest.webservices.restfulwebservices.user.User;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

@Component
public class PostDaoService {
    private static List<Post> posts = new ArrayList<>();
    private static int postCounter = 3;

    static{
        posts.add(new Post(1, new User(1, "Adam", new Date()), "Hola."));
        posts.add(new Post(2, new User(2, "Jim", new Date()), "Nos fuimos."));
        posts.add(new Post(3, new User(3, "Eve", new Date()), "Cada dia hace mas calor."));
    }

    public List<Post> findAll() { return posts; }

    public List<Post> findAllPostByUser(Integer idUser) {
        return posts.stream()
                .filter(post -> post.getUser().getId() == idUser)
                .collect(Collectors.toList());
    }

    public Post save(Post post) {
        if(post.getId_post() == null) {
            post.setId_post(++postCounter);
        }
        posts.add(post);
        return post;
    }

    public Post findOne(int idUser, int idPost) {
        for (Post post : posts) {
            if(post.getUser().getId() == idUser && post.getId_post() == idPost) {
                return post;
            }
        }
        return null;
    }
}
